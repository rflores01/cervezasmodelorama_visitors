const token = 'WcKHBBk001PLvWsCeHfTlEaQwhO98JplGmcvkQaeRCGY9TrozZOOE4AslNKDvuyBi9a3VOWrQUF7HVtyg5BOG6cV9LvXNRs0SjYyrhexINmAIUVA0c8BUx6MUWibE39jHUGmPE0VoZbr7oxMPyW7r50aApnXR7ZuXMEh7DLI7Y4srlbEPpCs99k87ocENCL246RfyaFz1622941446';

const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  });

window.onload = () => {
    const body = document.body;
    const produceContainer = document.querySelector('#produce-container');
    const produceSubContainer = document.querySelector('#produce-sub-container');

    var eventSrc = new EventSource('http://localhost/cervezasmodelorama-order-event.php');
        eventSrc.onmessage = (e) => {
            var data = JSON.parse(e.data);
            console.log(data,'<---');

            Toast.fire({
                icon: 'success',
                title: data.name.trim(),
                text: 'order update!'
              });

            var n = new Notification(data.name + ' update status',{
                body: 'please view order list',
                icon: 'https://arvolution.com/wp-content/uploads/2019/03/grupo_modelo_logo-e1553570073779.png'
            });

            clearFile();
        };

    /** NOTIFICATION PERMITION */
    if(Notification.permission !== 'denied')
    {
        Notification.requestPermission(permission => {
            //
        });
    }
    
    var swiper = new Swiper(".mySwiper", {
        pagination: {
            el: ".swiper-pagination",
            type: "progressbar",
        },
        autoplay: {
            delay: 5000,
            disableOnInteraction: false,
        },
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
      });

    /** OPEN MODAL CART */
    var btnCart = document.querySelector('#view-product-status');
        btnCart.onclick = () => {
            $('#myModal').modal('show');
            getOrders();
        };

    /** GET PRODUCE */
    fetch('http://localhost/cervezasmodelorama/public/api/getProduce', {
        method: 'GET',
        headers: {
            'api-token': token
        }
    }).then(rs => rs.json()).then(rs => {
        if(rs.action != false)
        {
            rs.data.map((el,n) => {

                var card = hex.div('',('ui card').split(' '),produceSubContainer);
                    card.style.margin = '0 0 2em 0';
                    card.style.display = 'none';
                var label = hex.span('',('ui corner blue label').split(' '), card);
                var cash = hex.i('',('dollar sign icon').split(' '),label);
                var image = hex.div('',['image'],card);
                    image.style.display = 'flex';
                    image.style.justifyContent = 'center';
                    image.style.height = '20em';
                    image.style.background = 'url('+el.image+')';
                    image.style.backgroundSize = 'contain';
                    image.style.backgroundRepeat = 'no-repeat';
                    image.style.backgroundPosition = 'center';
                // var img = hex.img('',[],image);
                //     img.src = el.image;
                //     img.style.width = '100%';
                var content = hex.div('',['content'],card);
                var title = hex.span(el.name,['header'],content);
                var description = hex.span(el.description,['description'],content);
                var container = hex.div('',[],content);
                    container.style.margin = '0.5em 0';
                    container.style.display = 'flex';
                    container.style.justifyContent = 'space-between';
                var price = hex.div('$'+el.price,('ui grey label').split(' '),container);
                var rankingDiv = hex.div('',[],container);
                for(var i = 0; i < 5; i++)
                {
                    if(i < el.ranking)
                    {
                        hex.i('',('star grey icon').split(' '),rankingDiv);
                    }
                    else
                    {
                        hex.i('',('star outline grey icon').split(' '),rankingDiv);
                    }
                }
                var extracontent = hex.div('',('extra content').split(' '), card);
                    extracontent.style.display = 'flex';
                    extracontent.style.justifyContent = 'space-between';
                    extracontent.style.alignItems = 'center';

                var inputcontent = hex.div('',('ui icon input').split(' '),extracontent);
                    inputcontent.style.width = '10em';
                var input = hex.input('',[],inputcontent);
                    input.type = 'number';
                    input.placeholder = 'amount';
                    input.min = 1;
                    input.max = 100;
                var icon = hex.i('',('dollar sign icon').split(' '),inputcontent);
                
                var btnSave = hex.button('',('ui button').split(' '),extracontent);
                var str = hex.span('require',[],btnSave);
                    str.style.marginRight = '0.5em';
                var icon = hex.i('',('send icon').split(' '),btnSave);

                    btnSave.onclick = (e) => {

                        if(input.value.trim().length == 0)
                        {
                            input.classList.add('error');
                            return false;
                        }
                        else
                        {
                            input.classList.remove('error');
                        }

                        el['total'] = input.value.trim();
                        el['amount'] = parseFloat(el.price) * parseFloat(input.value);
                        el['user_id'] = 1;

                        sendOrder(el);
                    };

                if((n+1) == rs.data.length)
                {
                    $('#produce-sub-container .card').transition({
                        animation : 'zoom',
                        duration  : 500,
                        interval  : 200
                      });
                }
            });
        }
    });

};

function sendOrder(obj) {
    var formData = new FormData();
        formData.append('product',JSON.stringify(obj));
    
    fetch('http://localhost/cervezasmodelorama/public/api/saveOrder',{
        method: 'POST',
        mode: 'cors',
        headers: {
            'api-token': token,
        },
        body: formData
    }).then(rs => rs.json()).then(rs => {
        if(rs.action != false)
        {
            $('#cart-item').transition('zoom');

            Toast.fire({
                icon: 'success',
                title: 'order create!'
              });
        }
        else
        {
            Toast.fire({
                icon: 'error',
                title: 'error',
                text: rs.msg
              });
        }
    });

}

function getOrders() {
    var viewproduct = document.querySelector('#view-product');
    fetch('http://localhost/cervezasmodelorama/public/api/orders/'+1,{
        method: 'GET',
        headers: {
            'api-token': token
        }
    }).then(rs => rs.json()).then(rs => {

        $('#dimmer-loader').removeClass('active');
        var tbody = document.querySelector('#tbl-orders tbody'); 
            tbody.innerHTML = '';

        rs.data.map(el => {
            var tr = hex.tr('',[],tbody);
            
            hex.td(el.name,[],tr);
            hex.td(el.price,[],tr);
            hex.td(el.amount,[],tr);
            hex.td(el.total,[],tr);
            hex.td(el.delivery,[],tr);
        });
    });
}

function clearFile() {
    fetch('http://localhost/cervezasmodelorama/public/api/clearFile2',{
        method: 'GET',
        headers: {
            'api-token': token
        },
    }).then(rs => rs.json()).then(rs => {
        if(rs.action != false)
        {
            console.log('file clear!','<<<<');
        }
    });
}